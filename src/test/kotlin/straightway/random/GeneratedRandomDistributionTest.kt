/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.random

import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.flow.Equal
import straightway.testing.flow.expect
import straightway.testing.flow.is_
import straightway.testing.flow.to_

class GeneratedRandomDistributionTest {

    @Test
    fun `values are converted`() =
            Given {
                object {
                    val sut = GeneratedRandomDistribution(arrayOf<Byte>(1, 2, 3).iterator(), 1) {
                        "b${single()}"
                    }
                }
            } when_ {
                sut.joinToString(",")
            } then {
                expect(it.result is_ Equal to_ "b1,b2,b3")
            }
}